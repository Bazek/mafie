import 'dart:convert';

import 'package:http/http.dart' as http;

import "session.dart";


class Api {
  static const String url = String.fromEnvironment("API_URL", defaultValue: "http://localhost:3481");

  static Map<String, String> getHeaders() {
    return {
      "Content-Type": "application/json",
      "X-Session-Id": Session.id.value == null ? "" : Session.id.value!,
      "X-Session-Token": Session.token.value == null ? "" : Session.token.value!,
    };
  }

  static http.Response processResponse(http.Response response) {
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    print('Response headers: ${response.headers}');
    if (response.statusCode == 401 || response.statusCode == 403) {
      Session.setId(null);
      Session.setToken(null);
    }
    else if (response.headers["x-session-id"] != null && response.headers["x-session-token"] != null) {
      print("Session");
      Session.setId(response.headers["x-session-id"]);
      Session.setToken(response.headers["x-session-token"]);
    }
    return response;
  }

  static Future<http.Response> get(String path) async {
    http.Response response = await http.get(Uri.parse(url + path), headers: getHeaders());
    return processResponse(response);
  }

  static Future<http.Response> post(String path, Object? body, {List<String> files = const []}) async {
    http.Response response;
    if (files.isEmpty) {
      response = await http.post(Uri.parse(url + path), headers: getHeaders(), body: json.encode(body));
    } else {
      var request = http.MultipartRequest('POST', Uri.parse(url));
      for (var file in files) {
        request.files.add(await http.MultipartFile.fromPath(file, file));
      }
      response = (await request.send()) as http.Response;
    }
    return processResponse(response);
  }

  static Future<http.Response> put(String path, Object? body) async {
    http.Response response = await http.put(Uri.parse(url + path), headers: getHeaders(), body: json.encode(body));
    return processResponse(response);
  }

  static Future<http.Response> patch(String path, Object? body) async {
    http.Response response = await http.patch(Uri.parse(url + path), headers: getHeaders(), body: json.encode(body));
    return processResponse(response);
  }

  static Future<http.Response> delete(String path) async {
    http.Response response = await http.delete(Uri.parse(url + path), headers: getHeaders());
    return processResponse(response);
  }
}
