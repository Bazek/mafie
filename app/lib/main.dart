import 'package:flutter/material.dart';

import 'game/page.dart';
import 'main/page.dart';
import 'session.dart';


void main() {
  Session.init();
  runApp(const Mafie());
}


class Mafie extends StatefulWidget {
  const Mafie({Key? key}) : super(key: key);

  @override
  State<Mafie> createState() => MafieState();
}

class MafieState extends State<Mafie> with SingleTickerProviderStateMixin {
  final MainPage mainPage = const MainPage();
  final GamePage gamePage = const GamePage();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mafie',
      theme: ThemeData(
        scaffoldBackgroundColor: const Color(0xffcc9933),
      ),
      home: ValueListenableBuilder<String?>(
        valueListenable: Session.id,
        builder: (BuildContext context, String? value, Widget? child) {
          return Session.id.value == null ? mainPage : gamePage;
        },
      ),
    );
  }
}
