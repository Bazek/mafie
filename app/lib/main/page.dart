import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'tabs/info.dart';
import 'tabs/login.dart';
import 'tabs/lost_password.dart';
import 'tabs/register.dart';


class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => MainPageState();
}

class MainPageState extends State<MainPage> with SingleTickerProviderStateMixin {
  late final TabController tabController = TabController(length: 4, initialIndex: 1, vsync: this);
  late final FlutterSecureStorage storage = const FlutterSecureStorage();

  void Function() tabSwitcher(int index) {
    return () { tabController.index = index; };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0.0,
        backgroundColor: const Color(0xffc08030),
        foregroundColor: Colors.black,
        title: const Text('PODSVĚTÍ BRNO'),
        centerTitle: true,
        bottom: TabBar(
          controller: tabController,
          indicator: const BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
            color: Color(0xffc09030),
          ),
          labelColor: Colors.black,
          isScrollable: true,
          tabs: const <Widget>[
            Tab(icon: Icon(Icons.info_outlined), text: "ÚVOD"),
            Tab(icon: Icon(Icons.supervised_user_circle_outlined), text: "PŘIHLÁŠENÍ"),
            Tab(icon: Icon(Icons.app_registration_outlined), text: "REGISTRACE"),
            Tab(icon: Icon(Icons.password_outlined), text: "ZAPOMENUTÉ HESLO"),
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          infoTab,
          LoginTab(tabSwitcher: tabSwitcher),
          RegisterTab(tabSwitcher: tabSwitcher),
          LostPasswordTab(tabSwitcher: tabSwitcher),
        ],
      ),
    );
  }
}
