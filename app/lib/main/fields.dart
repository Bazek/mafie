import 'package:flutter/material.dart';


class MainFormField extends StatelessWidget {
  final TextEditingController? controller;
  final String? value;
  final String? labelText;
  final bool obscureText;
  final bool readOnly;
  final bool enabled;
  final String? Function(String?)? validator;
  const MainFormField(this.labelText, {
    Key? key, this.controller, this.value,
    this.obscureText = false, this.readOnly = false, this.enabled = true,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(80, 12, 80, 12),
      child: TextFormField(
        initialValue: value,
        controller: controller,
        obscureText: obscureText,
        readOnly: readOnly,
        enabled: enabled,
        style: const TextStyle(fontSize: 20),
        decoration: InputDecoration(
          border: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
          labelText: labelText,
          floatingLabelStyle: const TextStyle(color: Color(0xff304040)),
        ),
        cursorColor: const Color(0xff304040),
        validator: validator,
      ),
    );
  }
}


class MainFormDateField extends StatelessWidget {
  final TextEditingController controller;
  final String? value;
  final String? labelText;
  final String? Function(String?)? validator;
  const MainFormDateField(this.labelText, {Key? key, required this.controller, this.value, this.validator}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(80, 12, 80, 12),
      child: TextFormField(
        initialValue: value,
        controller: controller,
        style: const TextStyle(fontSize: 20),
        decoration: InputDecoration(
          border: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
          labelText: labelText,
          floatingLabelStyle: const TextStyle(color: Color(0xff304040)),
        ),
        readOnly: true,
        showCursor: false,
        onTap: () {
          showDatePicker(
            context: context,
            firstDate: DateTime(1900),
            initialDate: DateTime(2000),
            lastDate: DateTime(2020),
          ).then((value) {
            if (value != null) {
              controller.text = value.toString().split(" ")[0];
            }
          });
        },
        validator: validator,
      ),
    );
  }
}


class MainFormButton extends StatelessWidget {
  final String text;
  final void Function()? onPressed;
  const MainFormButton(this.text, {Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 68,
      padding: const EdgeInsets.fromLTRB(80, 12, 80, 12),
      child: ElevatedButton(
        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xff304040))),
        child: Text(text, style: const TextStyle(fontSize: 20)),
        onPressed: onPressed,
      ),
    );
  }
}


class MainFormTextButton extends StatelessWidget {
  final String text;
  final void Function()? onPressed;
  const MainFormTextButton(this.text, {Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(text, style: const TextStyle(fontSize: 16, color: Color(0xff304040))),
      onPressed: onPressed,
    );
  }
}


class MainFormHintButton extends StatelessWidget {
  final String text;
  final String buttonText;
  final void Function()? onPressed;
  const MainFormHintButton(this.text, this.buttonText, {Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(text, style: const TextStyle(fontSize: 16)),
        TextButton(
          child: Text(buttonText, style: const TextStyle(fontSize: 20, color: Color(0xff304040))),
          onPressed: onPressed,
        )
      ],
    );
  }
}
