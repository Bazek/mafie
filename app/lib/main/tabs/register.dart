import 'dart:convert';

import 'package:flutter/material.dart';

import '../../api.dart';
import '../../session.dart';
import '../../main/form.dart';
import '../../main/fields.dart';


class RegisterTab extends StatefulWidget {
  final void Function() Function(int) tabSwitcher;
  const RegisterTab({Key? key, required this.tabSwitcher}) : super(key: key);

  @override
  State<RegisterTab> createState() => RegisterTabState();
}

class RegisterTabState extends State<RegisterTab> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  static TextEditingController nameController = TextEditingController();
  static TextEditingController birthdateController = TextEditingController();
  static TextEditingController password1Controller = TextEditingController();
  static TextEditingController password2Controller = TextEditingController();

  static String? validateName(String? value) {
    if (value == null || value.isEmpty) {
      return 'Vyplň svoje jméno';
    }
    return null;
  }

  static String? validateBirthdate(String? value) {
    if (value == null || value.isEmpty) {
      return 'Vyplň svoje datum narození';
    }
    return null;
  }

  static String? validatePassword1(String? value) {
    if (value == null || value.length < 8) {
      return 'Heslo musí mít alespoň 8 znaků';
    }
    return null;
  }

  static String? validatePassword2(String? value) {
    if (value != password1Controller.text) {
      return "Hesla se neshodují";
    }
    return null;
  }

  void submitForm() async {
    if (formKey.currentState!.validate()) {
      var response = await Api.post("/users", {
          "name": nameController.text,
          "password": password1Controller.text,
          "birthdate": birthdateController.text,
      });
      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            backgroundColor: Color(0xff1f7f3f),
            content: Text('Nový mafián byl úspěšně zaregistrován'),
          ),
        );
        Session.setUserId(jsonDecode(response.body)["id"].toString());
        Session.setLogin(jsonDecode(response.body)["id"].toString());
        Session.setPassword(password1Controller.text);
        widget.tabSwitcher(1)();
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Color(0xff9f1f1f),
          content: Text('Registrace se nezdařila'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: MainForm("Registrace nového mafiána",
      formKey: formKey,
      children: <Widget> [
        MainFormField("Jméno mafiána", controller: nameController, validator: validateName),
        MainFormDateField("Datum narození", controller: birthdateController, validator: validateBirthdate),
        MainFormField("Heslo", obscureText: true, controller: password1Controller, validator: validatePassword1),
        MainFormField('Heslo znovu', obscureText: true, controller: password2Controller, validator: validatePassword2),
        MainFormButton("Registrace", onPressed: submitForm),
        MainFormHintButton('Už máš vytvořený mafiánský účet?', 'Přihlas se', onPressed: widget.tabSwitcher(1)),
      ],
    ));
  }
}
