import 'package:flutter/material.dart';

import '../../main/form.dart';
import '../../main/fields.dart';


class LostPasswordTab extends StatefulWidget {
  final void Function() Function(int) tabSwitcher;
  const LostPasswordTab({Key? key, required this.tabSwitcher}) : super(key: key);

  @override
  State<LostPasswordTab> createState() => LostPasswordTabState();
}

class LostPasswordTabState extends State<LostPasswordTab> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController idController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  void submitForm() async {
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: MainForm("Obnova ztraceného hesla",
      formKey: formKey,
      children: <Widget> [
        MainFormField("Přihlašovací ID", controller: idController, validator: null),
        MainFormField("Potvrzený email", controller: emailController, validator: null),
        MainFormField("Potvrzený telefon", controller: phoneController, validator: null),
        MainFormButton("Registrace", onPressed: submitForm),
        MainFormHintButton('Ještě nemáš mafiánský účet?', 'Zaregistruj se', onPressed: widget.tabSwitcher(2)),
      ],
    ));
  }
}