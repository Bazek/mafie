import 'package:flutter/material.dart';


const infoTab = SingleChildScrollView(
    child: Text(
        """O co jde ve hře Mafie Brno

Jaké je to stát se na chvíli vrahem a zároveň hledanou obětí? Jaký je to pocit? 

Pojďte si s námi užít trochu dobrodružství a oživit stará i nová přátelství. 

Mafie Brno je příjemnou adrenalinovou injekcí do dlouhých městských dnů, nevšední a časově nenáročnou recesní hrou, probíhající na pozadí Vašeho běžného života. Mafie prověří Vaši bdělost, odvahu, strategii a mnohdy zapojí i Vaše přátele.


Hlavními partnery jsou:

Kino ART
Masarykova univerzita


Naše bratrské mafiánské klany:

Mafie - Praha - náš bratrský mafiánský klan
Mafiánský info koutek - historky, reporty a jiné zajímavosti z pražského podsvětí... 
Assassin Brno - naše spřátelená organizace se stejným cílem


Pořádající organizace: 

Ruchadlo – občanské sdružení, zaštiťující hru Mafie Brno


Kontakt na organizátory hry Mafie Brno

brno@podsveti.cz""",
    ),
);
