import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../api.dart';
import '../../session.dart';
import '../../main/form.dart';
import '../../main/fields.dart';


class LoginTab extends StatefulWidget {
  final void Function() Function(int) tabSwitcher;
  const LoginTab({Key? key, required this.tabSwitcher}) : super(key: key);

  @override
  State<LoginTab> createState() => LoginTabState();
}

class LoginTabState extends State<LoginTab> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  static TextEditingController loginController = TextEditingController(text: Session.login.value);
  static TextEditingController passwordController = TextEditingController(text: Session.password.value);

  void loginListener() { loginController.text = Session.login.value != null ? Session.login.value! : ""; }
  void passwordListener() { passwordController.text = Session.password.value != null ? Session.password.value! : ""; }

  @override
  initState() {
    super.initState();
    // Add listeners to this class
    Session.login.addListener(loginListener);
    Session.password.addListener(passwordListener);
  }

  @override
  dispose() {
    super.dispose();
    // Remove listeners from this class
    Session.login.removeListener(loginListener);
    Session.password.removeListener(passwordListener);
  }

  void submitForm() async {
    http.Response response = await Api.post("/sessions", {"login": loginController.text, "password": passwordController.text});
    if (response.statusCode == 200) {
      Session.setUserId(jsonDecode(response.body)["user"]["id"].toString());
      Session.setId(jsonDecode(response.body)["id"].toString());
      Session.setToken(jsonDecode(response.body)["token"].toString());
      Session.setLogin(loginController.text);
      Session.setPassword(passwordController.text);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Color(0xff1f7f3f),
          content: Text('Úspěšně přihlášen'),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Color(0xff9f1f1f),
          content: Text('Přihlášení se nepovedlo. Zapomněl jsi heslo?'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: MainForm("Přihlášení mafiána",
      formKey: formKey,
      children: <Widget> [
        MainFormField("Přihlašovací ID/email/telefon", controller: loginController),
        MainFormField("Heslo", obscureText: true, controller: passwordController),
        MainFormButton("Přihlásit", onPressed: submitForm),
        MainFormTextButton("Zapomenuté heslo", onPressed: widget.tabSwitcher(3)),
        MainFormHintButton('Ještě nemáš mafiánský účet?', 'Zaregistruj se', onPressed: widget.tabSwitcher(2)),
      ],
    ));
  }
}