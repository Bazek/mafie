import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class Storage {
  static FlutterSecureStorage secureStorage = const FlutterSecureStorage();
}


class Session extends Storage {
  static ValueNotifier<String?> userId = ValueNotifier<String?>(null);
  static ValueNotifier<String?> id = ValueNotifier<String?>(null);
  static ValueNotifier<String?> token = ValueNotifier<String?>(null);
  static ValueNotifier<String?> login = ValueNotifier<String?>(null);
  static ValueNotifier<String?> password = ValueNotifier<String?>(null);

  static void init() {
    Storage.secureStorage.read(key: "sessionUserId").then((String? value){userId.value = value;});
    Storage.secureStorage.read(key: "sessionId").then((String? value){id.value = value;});
    Storage.secureStorage.read(key: "sessionToken").then((String? value){token.value = value;});
    Storage.secureStorage.read(key: "sessionLogin").then((String? value){login.value = value;});
    Storage.secureStorage.read(key: "sessionPassword").then((String? value){password.value = value;});
  }

  static void setUserId(String? value) {
    Storage.secureStorage.write(key: "sessionUserId", value: value).then((v){userId.value = value;});
  }

  static void setId(String? value) {
    Storage.secureStorage.write(key: "sessionId", value: value).then((v){id.value = value;});
  }

  static void setToken(String? value) {
    Storage.secureStorage.write(key: "sessionToken", value: value).then((v){token.value = value;});
  }

  static void setLogin(String? value) {
    Storage.secureStorage.write(key: "sessionLogin", value: value).then((v){login.value = value;});
  }

  static void setPassword(String? value) {
    Storage.secureStorage.write(key: "sessionPassword", value: value).then((v){password.value = value;});
  }
}
