import 'package:flutter/material.dart';

import "package:Mafie/game/tabs/forum.dart";
import "package:Mafie/game/tabs/user.dart";


class GamePage extends StatefulWidget {
  const GamePage({Key? key}) : super(key: key);

  @override
  State<GamePage> createState() => GamePageState();
}

class GamePageState extends State<GamePage> with SingleTickerProviderStateMixin {
  late final TabController tabController = TabController(length: 2, vsync: this);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0.0,
        backgroundColor: const Color(0xffc08030),
        foregroundColor: Colors.black,
        title: const Text('PODSVĚTÍ BRNO'),
        centerTitle: true,
        bottom: TabBar(
          controller: tabController,
          indicator: const BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
            color: Color(0xffc09030),
          ),
          labelColor: Colors.black,
          isScrollable: true,
          tabs: const <Widget>[
            Tab(icon: Icon(Icons.info_outlined), text: "UŽIVATEL"),
            Tab(icon: Icon(Icons.forum_outlined), text: "FÓRUM"),
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: const [
          UserTab(),
          ForumTab(),
        ],
      ),
    );
  }
}
