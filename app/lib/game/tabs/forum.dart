import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../api.dart';


class ForumTab extends StatefulWidget {
  const ForumTab({Key? key}) : super(key: key);

  @override
  State<ForumTab> createState() => ForumTabState();
}

class ForumTabState extends State<ForumTab> {
  List<dynamic> forums = [];
  int? actualForumId;
  List<dynamic> posts = [];

  @override
  void initState() {
    super.initState();
    if (actualForumId == null) {
      Api.get("/forums").then((response) {
        if (response.statusCode == 200) {
          setState(() {
            forums = jsonDecode(response.body)["items"];
          });
        }
      });
    } else {
    }
  }

  void onAdd(int? forumId) {
    TextEditingController nameController = TextEditingController();
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Nové fórum'),
          content: TextField(
            controller: nameController,
            decoration: const InputDecoration(hintText: "Zadejte název fóra"),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('CANCEL'),
              onPressed: () { Navigator.pop(context); },
            ),
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.pop(context);
                Api.post("/forums", {"name": nameController.text, "parent_id": forumId}).then((response) {
                  if (response.statusCode == 200) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        backgroundColor: Color(0xff1f7f3f),
                        content: Text('Úspěšně vytvořeno nové fórum'),
                      ),
                    );
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        backgroundColor: Color(0xff9f1f1f),
                        content: Text('Nepovedlo se vytvořit nové fórum'),
                      ),
                    );
                  }
                });
              },
            ),
          ],
        );
      },
    );
  }

  void onRemove(int? forumId) {
    true;
  }

  void onEnter(int? forumId) {
    setState(() {
      actualForumId = forumId;
      Api.get("/forums/$actualForumId/posts").then((response) {
        if (response.statusCode == 200) {
          setState(() {
            posts = jsonDecode(response.body)["items"];
          });
        }
      });
    });
  }

  void onSend(text) {
    Api.post("/forums/$actualForumId/posts", {"text": text}).then((response) {
      if (response.statusCode == 200) {
        setState(() {
          posts.add(jsonDecode(response.body));
        });
      }
    });
  }

  List<Widget> _forumsTree(List<dynamic> forums, {int? forumId, int depth = 0}) {
    List<Widget> widgets = [Row(mainAxisAlignment: MainAxisAlignment.end, children: [
      ElevatedButton(
        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xff509040))),
        child: const Text("Vstoupit", style: TextStyle(fontSize: 20)),
        onPressed: () => onEnter(forumId),
      ),
      ElevatedButton(
        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xff6870d0))),
        child: const Text("Nové", style: TextStyle(fontSize: 15)),
        onPressed: () => onAdd(forumId),
      ),
      /*ElevatedButton(
        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xffbf4030))),
        child: const Text("Smazat", style: TextStyle(fontSize: 15)),
        onPressed: () => onRemove(forumId),
      ),*/
    ])];
    for (var forum in forums) {
      widgets.add(ExpansionTile(
        title: Padding(
          padding: EdgeInsets.fromLTRB(depth * 40, 0, 0, 0),
          child: Text(forum["name"]),
        ),
        children: _forumsTree(forum["children"], forumId: forum["id"], depth: depth + 1),
      ));
    }
    return widgets;
  }

  List<Widget> _postsList(List<dynamic> posts, {int depth = 0}) {
    TextEditingController messageController = TextEditingController();
    List<Widget> widgets = [];
    for (var post in posts) {
      widgets.add(Padding(
          padding: EdgeInsets.fromLTRB(depth * 40, 0, 0, 0),
          child: ListTile(
            title: Text(post["user"]["name"]),
            subtitle: Text(post["text"]),
          ),
        ),
      );
    }
    widgets.add(Row(children: [
      const Text("Napiš:"),
      Flexible(child: TextField(
        controller: messageController,
        onSubmitted: onSend,
        style: const TextStyle(fontSize: 20),
        decoration: const InputDecoration(
          border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
          floatingLabelStyle: TextStyle(color: Color(0xff304040)),
        ),
      )),
      ElevatedButton(
        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xff304040))),
        child: const Text("Odeslat", style: TextStyle(fontSize: 20)),
        onPressed: () {
          onSend(messageController.text);
        },
      ),
    ]));
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    Widget body = (actualForumId == null) ?
      ListView(children: _forumsTree(forums)) :
      ListView(controller: ScrollController(initialScrollOffset: 100000000),children: _postsList(posts));
    return Scaffold(body: body);
  }
}
