import 'dart:convert';
import 'package:flutter/material.dart';

import '../../api.dart';
import '../../session.dart';
import '../../main/form.dart';
import '../../main/fields.dart';
import '../../main/tabs/register.dart';


class EmailPhoneVerified {
  late String? value; late bool verified;
  EmailPhoneVerified({this.value, this.verified = false});
}

typedef ListEmailPhoneVerified = List<EmailPhoneVerified>;
typedef ListEmailPhoneVerifiedNotifier = ValueNotifier<ListEmailPhoneVerified>;


class UserTab extends StatefulWidget {
  const UserTab({Key? key}) : super(key: key);

  @override
  State<UserTab> createState() => UserTabState();
}

class UserTabState extends State<UserTab> {
  static ValueNotifier<String?> pictureUrl = ValueNotifier<String?>(null);
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController idController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController birthdateController = TextEditingController();
  TextEditingController newEmailController = TextEditingController();

  void sessionRefresh() async {
    var response = await Api.get("/sessions/${Session.id.value}");
  }

  void sessionClose() async {
    var response = await Api.delete("/sessions/${Session.id.value}");
    Session.setId(null);
    Session.setToken(null);
  }

  String? emailValidator(String? value) {
    RegExp regExp = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    if (!regExp.hasMatch(value!)) {
      return 'Nevalidní email';
    }
    return null;
  }

  String? phoneValidator(String? value) {
    RegExp regExp = RegExp(r'^[+0][0-9]{12}$');
    if (!regExp.hasMatch(value!)) {
      return 'Nevalidní číslo. Příklad +420777123789456';
    }
    return null;
  }

  void submitForm() async {
    if (formKey.currentState!.validate()) {
      var response = await Api.patch('/users/${idController.text}', {
          "name": nameController.text,
          "birthdate": birthdateController.text,
        });
      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            backgroundColor: Color(0xff1f7f3f),
            content: Text('Mafián byl úspěšně uložen'),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Api.get("/users/${Session.userId.value}").then((response) {
      if (response.statusCode == 200) {
        var user = jsonDecode(response.body);
        idController.text = user["id"].toString();
        nameController.text = user["name"];
        birthdateController.text = user["birthdate"];
        pictureUrl.value = "${user["image"]["id"]}/${user["image"]["url"]}";
        EmailColumn.emails.value = [];
        for (var email in user["emails"]) {
          EmailPhoneVerified emailVerified = EmailPhoneVerified(value: email["email"]);
          for (var verification in email["verifications"]) {
            if (verification["verified"] != null) {
              emailVerified.verified = true;
            }
          }
          EmailColumn.emails.value.add(emailVerified);
        }
        PhoneColumn.phones.value = [];
        for (var phone in user["phones"]) {
          EmailPhoneVerified phoneVerified = EmailPhoneVerified(value: phone["phone"]);
          for (var verification in phone["verifications"]) {
            if (verification["verified"] != null) {
              phoneVerified.verified = true;
            }
          }
          PhoneColumn.phones.value.add(phoneVerified);
        }
      }
    });
    return Scaffold(body: SingleChildScrollView(child: Column(children: [
      MainForm("Přihlášený uživatel",
        formKey: formKey,
        children: <Widget> [
          MainFormField("ID", controller: idController, readOnly: true, enabled: false),
          MainFormField("Jméno", controller: nameController, validator: RegisterTabState.validateName),
          MainFormDateField("Datum narození", controller: birthdateController, validator: RegisterTabState.validateBirthdate),
          MainFormButton("Uložit", onPressed: submitForm),
      ]),
      Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
        child: const Text("Fotka", style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold)),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(child: ValueListenableBuilder<String?>(
            valueListenable: pictureUrl,
            builder: (BuildContext context, String? value, Widget? child) {
              if (pictureUrl.value != null) {
                return Image.network("${Api.url}/uploads/${pictureUrl.value}", width: 140, height: 180, fit: BoxFit.cover);
              } else {
                return Image.network("${Api.url}/uploads/${pictureUrl.value}");
              }
            },
          )),
          Column(
            children: [
              TextButton(onPressed: () {  }, child: const Text("Nová")),
              TextButton(onPressed: () {  }, child: const Text("Ořez")),
              TextButton(onPressed: () {  }, child: const Text("Smazat")),
            ],
          ),
        ],
      ),
      Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
        child: const Text("E-mailové adresy", style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold)),
      ),
      EmailColumn(validator: emailValidator),
      Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 5),
        child: const Text("Telefonní čísla", style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold)),
      ),
      PhoneColumn(validator: phoneValidator),
      ValueListenableBuilder(
          valueListenable: Session.id,
          builder: (BuildContext context, String? value, Widget? child) {
            return Text("SessionID: $value");
          }
      ),
      ValueListenableBuilder(
          valueListenable: Session.token,
          builder: (BuildContext context, String? value, Widget? child) {
            return Text("Token: $value");
          }
      ),
      MainFormButton("Session refresh", onPressed: sessionRefresh),
      MainFormButton("Session close", onPressed: sessionClose),
    ])));
  }
}


class EmailPhoneForm extends StatelessWidget {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final EmailPhoneVerified emailPhoneVerified;
  final String? Function(String?) validator;
  final void Function(String) onAdd;
  final void Function(String) onVerify;
  final void Function(String) onRemove;
  EmailPhoneForm(this.emailPhoneVerified, {
    Key? key, required this.validator,
    required this.onAdd, required this.onVerify, required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController textController = TextEditingController(text: emailPhoneVerified.value);
    List<Widget> widgets = [Expanded(child: TextFormField(
      enabled: (emailPhoneVerified.value == null),
      readOnly: (emailPhoneVerified.value != null),
      controller: textController,
      style: const TextStyle(fontSize: 20),
      decoration: const InputDecoration(
        isDense: true, // important line
        contentPadding: EdgeInsets.all(5),// control your hints text size
        border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
      ),
      cursorColor: const Color(0xff304040),
      validator: validator,
    ))];
    if (emailPhoneVerified.value == null) {
      widgets.add(IconButton(
        icon: const Icon(Icons.add, size: 36.0),
        padding: const EdgeInsets.all(0.0),
        splashRadius: 16.0,
        onPressed: () => {if (formKey.currentState!.validate()) onAdd(textController.text)},
      ));
    } else {
      if (emailPhoneVerified.verified) {
        widgets.add(IconButton(
          icon: const Icon(Icons.verified, size: 32.0, color: Color(0xff1f7f3f)),
          padding: const EdgeInsets.all(0.0),
          splashRadius: 16.0,
          onPressed: () => {},
        ));
      } else {
        widgets.add(IconButton(
          icon: const Icon(Icons.question_mark_rounded, size: 32.0, color: Color(0xff9f1f1f)),
          padding: const EdgeInsets.all(0.0),
          splashRadius: 16.0,
          onPressed: () => onVerify(textController.text),
        ));
      }
    }
    return Container(
      constraints: const BoxConstraints(maxWidth: 800),
      child: Form(key: formKey, child: Row(children: widgets)),
    );
  }
}


abstract class EmailPhoneColumn extends StatelessWidget {
  final ListEmailPhoneVerifiedNotifier values;
  final String? Function(String?) validator;
  final String url = "unknown";
  late BuildContext buildContext;
  EmailPhoneColumn({Key? key, required this.values, required this.validator}) : super(key: key);

  void onAdd(String value) {
    Api.put("/users/${Session.userId.value}/$url/$value", null).then((response) {
      if (response.statusCode == 200) {
        values.value += [EmailPhoneVerified(value: value)];
        ScaffoldMessenger.of(buildContext).showSnackBar(
          const SnackBar(
            backgroundColor: Color(0xff1f7f3f),
            content: Text('Úspěšně přidáno'),
          ),
        );
        onVerify(value);
      } else {
        ScaffoldMessenger.of(buildContext).showSnackBar(
          const SnackBar(
            backgroundColor: Color(0xff9f1f1f),
            content: Text('Nepovedlo se přidat'),
          ),
        );
      }
    });
  }

  void onVerify(String value) {
    TextEditingController codeController = TextEditingController();
    showDialog(
      context: buildContext,
      builder: (context) {
        return AlertDialog(
          title: const Text('Ověřovací kód'),
          content: TextField(
            controller: codeController,
            decoration: const InputDecoration(hintText: "Zadejte ověřovací kód"),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('CANCEL'),
              onPressed: () { Navigator.pop(context); },
            ),
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.pop(context);
                Api.post("/users/${Session.userId.value}/$url/$value/verify/${codeController.text}", null).then((response) {
                  if (response.statusCode == 200) {
                    ScaffoldMessenger.of(buildContext).showSnackBar(
                      const SnackBar(
                        backgroundColor: Color(0xff1f7f3f),
                        content: Text('Úspěšně ověřeno'),
                      ),
                    );
                    ListEmailPhoneVerified newValues = [];
                    for (var oldValue in values.value) {
                      if (oldValue.value == value) {
                        oldValue.verified = true;
                      }
                      newValues.add(oldValue);
                    }
                    values.value = newValues;
                  } else {
                    ScaffoldMessenger.of(buildContext).showSnackBar(
                      const SnackBar(
                        backgroundColor: Color(0xff9f1f1f),
                        content: Text('Nepovedlo se ověřit'),
                      ),
                    );
                  }
                });
              },
            ),
          ],
        );
      },
    );
  }

  void onRemove(String value) {
  }

  @override
  Widget build(BuildContext context) {
    buildContext = context;
    return Container(
      constraints: const BoxConstraints(maxWidth: 800),
      padding: const EdgeInsets.fromLTRB(100, 10, 100, 10),
      child: ValueListenableBuilder<ListEmailPhoneVerified>(
        valueListenable: values,
        builder: (BuildContext context, ListEmailPhoneVerified listValues, Widget? child) {
          List<Widget> widgets = [];
          for (var value in listValues) {
            widgets.add(EmailPhoneForm(EmailPhoneVerified(value: value.value, verified: value.verified), validator: validator, onAdd: onAdd, onVerify: onVerify, onRemove: onRemove));
          }
          widgets.add(EmailPhoneForm(EmailPhoneVerified(), validator: validator, onAdd: onAdd, onVerify: onVerify, onRemove: onRemove));
          return Column(
            children: widgets,
          );
        },
      ),
    );
  }
}

class EmailColumn extends EmailPhoneColumn {
  static ListEmailPhoneVerifiedNotifier emails = ListEmailPhoneVerifiedNotifier([]);
  @override final String url = "emails";
  EmailColumn({Key? key, required validator}) : super(key: key, values: emails, validator: validator);
}

class PhoneColumn extends EmailPhoneColumn {
  static ListEmailPhoneVerifiedNotifier phones = ListEmailPhoneVerifiedNotifier([]);
  @override final String url = "phones";
  PhoneColumn({Key? key, required validator}) : super(key: key, values: phones, validator: validator);
}
