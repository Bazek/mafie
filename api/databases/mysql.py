import os

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker


MASTER_URL = os.environ.get("MYSQL_MASTER_URL") or os.environ.get("MYSQL_URL")
SLAVE_URL = os.environ.get("MYSQL_SLAVE_URL") or os.environ.get("MYSQL_URL")

ECHO = bool(os.environ.get("MYSQL_ECHO"))

master_engine = create_async_engine(MASTER_URL, echo=ECHO, pool_pre_ping=True, pool_recycle=300)
slave_engine = create_async_engine(SLAVE_URL, echo=ECHO)

MasterSession = sessionmaker(master_engine, AsyncSession, autoflush=True, autocommit=False, expire_on_commit=False)
SlaveSession = sessionmaker(slave_engine, AsyncSession, autoflush=True, autocommit=False, expire_on_commit=False)


# Dependency for FastAPI
async def get_master_session() -> AsyncSession:
    async with MasterSession() as session:
        yield session


async def get_slave_session() -> AsyncSession:
    async with SlaveSession() as session:
        yield session
