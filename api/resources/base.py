import fastapi
import pydantic

import models.base


class Meta(pydantic.BaseModel):
    count: int
    limit: int | None
    nextItems: bool
    offset: int | None
    previousItems: bool


async def get_many(
        transaction,
        model_class: models.base.ModelBase,
        filter_dict=None,
        order=None,
        limit=None,
        offset=None,
) -> dict:
    filter_dict = filter_dict or {}
    items, count = await model_class.get_many(transaction, filter_dict, order, limit, offset)
    return {
        "items": items,
        "meta": {
            "count": count,
            "limit": limit,
            "nextItems": limit is not None and (offset or 0) + limit < count,
            "offset": offset,
            "previousItems": count and (offset or 0) > 0,
        },
    }


async def get_one(
        transaction,
        model_class: models.base.ModelBase,
        item_id: str, id_field: str = "id",
) -> models.base.ModelBase:
    item = await model_class.get_one(transaction, item_id, id_field)
    if item is not None:
        return item
    raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
