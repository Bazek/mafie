import datetime
import typing

import fastapi
import pydantic
import sqlalchemy

import messengers.smtp
import messengers.twilio
import databases.mysql

import models.session
import models.uploads
import models.user
import resources.base


router = fastapi.APIRouter(tags=["user"])


class Verification(pydantic.BaseModel, orm_mode=True):
    generated: datetime.datetime = pydantic.Field(example=datetime.datetime(2022, 6, 19, 8, 30, 25))
    verified: datetime.datetime | None = pydantic.Field(example=datetime.datetime(2022, 6, 19, 8, 35, 25))


class Email(pydantic.BaseModel, orm_mode=True):
    email: pydantic.EmailStr = pydantic.Field(example="gonzales@mafie.cz")
    verifications: typing.List[Verification] = pydantic.Field()


class Phone(pydantic.BaseModel, orm_mode=True):
    phone: str = pydantic.Field(example="+420777341341")
    verifications: typing.List[Verification] = pydantic.Field()


class Image(pydantic.BaseModel, orm_mode=True):
    id: int = pydantic.Field(example=12)
    url: str = pydantic.Field(example="gi45u9rhe7we6rj5j5df")


class UserPublic(pydantic.BaseModel, orm_mode=True):
    id: int = pydantic.Field(example=123)
    name: str = pydantic.Field(example="Gonzales")
    image: typing.Optional[Image] = pydantic.Field()


class User(UserPublic):
    registered: datetime.datetime = pydantic.Field(example=datetime.datetime(2022, 6, 19, 8, 30, 25))
    birthdate: datetime.date = pydantic.Field(example=datetime.date(2022, 6, 19))
    emails: typing.List[Email] = pydantic.Field()
    phones: typing.List[Phone] = pydantic.Field()


class UserGetMany(pydantic.BaseModel):
    meta: resources.base.Meta = pydantic.Field()
    items: typing.List[User] = pydantic.Field()


class UserPost(pydantic.BaseModel, extra=pydantic.Extra.forbid):
    name: str = pydantic.Field(example="Gonzales")
    password: str = pydantic.Field(example="strong-pa$$word")
    birthdate: datetime.date = pydantic.Field(example=datetime.date(2022, 6, 19))


class UserPatch(pydantic.BaseModel, extra=pydantic.Extra.forbid):
    name: typing.Optional[str] = pydantic.Field(example="Gonzales The Killer")
    password: typing.Optional[str] = pydantic.Field(example="strong-pa$$word")
    birthdate: typing.Optional[datetime.date] = pydantic.Field(example=datetime.date(2022, 6, 19))
    image_id: typing.Optional[int] = pydantic.Field(example=23)


@router.post("/", response_model=User)
async def user_post(post: UserPost) -> models.user.User:
    user = models.user.User(**post.dict(exclude_unset=True), image=None, emails=[], phones=[])
    user.password = models.user.User.hash_password(user.password)
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(user)
    return user


@router.patch("/{user_id}", response_model=User)
async def user_patch(user_id: str, patch: UserPatch) -> models.user.User:
    if patch.image_id:
        async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
            image = (await slave_transaction.execute(sqlalchemy.select(models.uploads.Image).where(sqlalchemy.and_(
                models.uploads.Image.user_id == user_id,
                models.uploads.Image.id == patch.image_id,
            )))).scalars().first()
        if not image:
            raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        user = await models.user.User.get_one(master_transaction, user_id)
        for key, value in patch.dict(exclude_unset=True).items():
            if key == "password":
                value = models.user.User.hash_password(value)
            setattr(user, key, value)
    return user


@router.get("/", response_model=UserGetMany)
async def user_get_many() -> dict:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        return await resources.base.get_many(slave_transaction, models.user.User)


@router.get("/{user_id}", response_model=User)
async def user_get_one(user_id: str) -> models.user.User:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        return await resources.base.get_one(slave_transaction, models.user.User, user_id)


# EMAILS/PHONES


@router.put("/{user_id}/emails/{email}", response_model=Email)
async def user_email_put(user_id: int, email: pydantic.EmailStr) -> models.user.UserEmail:
    email = models.user.UserEmail(user_id=user_id, email=email)
    verification = models.user.UserEmailVerification()
    email.verifications.append(verification)
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(email)
    await messengers.smtp.smtp.send(email.email, "Ověření emailu", f"Ověřovací kód: {verification.code}")
    return email


@router.post("/{user_id}/emails/{email}/verify/{code}", response_model=Verification)
async def user_email_verify(user_id: int, email: pydantic.EmailStr, code: str) -> models.user.UserEmailVerification:
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        email_verification = (await master_transaction.execute(sqlalchemy.select(models.user.UserEmailVerification).where(
            sqlalchemy.and_(
                models.user.UserEmailVerification.user_id == user_id,
                models.user.UserEmailVerification.email == email,
                models.user.UserEmailVerification.code == code,
                models.user.UserEmailVerification.verified.is_(None),
                models.user.UserEmailVerification.generated > datetime.datetime.now() - datetime.timedelta(minutes=15),
            )
        ))).scalars().first()
        if not email_verification:
            raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
        email_verification.verified = datetime.datetime.now()
    return email_verification


@router.put("/{user_id}/phones/{phone}", response_model=Phone)
async def user_phone_put(user_id: int, phone: str) -> models.user.UserPhone:
    phone = models.user.UserPhone(user_id=user_id, phone=phone)
    verification = models.user.UserPhoneVerification()
    phone.verifications.append(verification)
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(phone)
    await messengers.twilio.twilio.send_sms(phone.phone, f"MAFIE: Ověřovací kód: {verification.code}")
    return phone


@router.post("/{user_id}/phones/{phone}/verify/{code}", response_model=Verification)
async def user_phone_verify(user_id: int, phone: str, code: str) -> models.user.UserPhoneVerification:
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        phone_verification = (await master_transaction.execute(sqlalchemy.select(models.user.UserPhoneVerification).where(
            sqlalchemy.and_(
                models.user.UserPhoneVerification.user_id == user_id,
                models.user.UserPhoneVerification.phone == phone,
                models.user.UserPhoneVerification.code == code,
                models.user.UserPhoneVerification.verified.is_(None),
                models.user.UserPhoneVerification.generated > datetime.datetime.now() - datetime.timedelta(minutes=15),
            )
        ))).scalars().first()
        if not phone_verification:
            raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
        phone_verification.verified = datetime.datetime.now()
    return phone_verification
