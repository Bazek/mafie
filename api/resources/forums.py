import datetime
import typing

import fastapi.responses
import pydantic

import databases.mysql
import models.session
import models.forum
import models.user
import resources.base
import resources.sessions
import resources.users


router = fastapi.APIRouter(tags=["forums"], dependencies=[resources.sessions.check_session_dependency])


class Forum(pydantic.BaseModel, orm_mode=True):
    id: int = pydantic.Field(example=123456)
    name: str = pydantic.Field(example="Forum")
    created: datetime.datetime = pydantic.Field(example=datetime.datetime(2022, 6, 19, 8, 30, 25))
    children: typing.List["Forum"] = pydantic.Field()


class ForumGetMany(pydantic.BaseModel):
    meta: resources.base.Meta = pydantic.Field()
    items: typing.List[Forum] = pydantic.Field()


class ForumPost(pydantic.BaseModel, extra=pydantic.Extra.forbid):
    name: str = pydantic.Field(example="Forum")
    parent_id: typing.Optional[int] = pydantic.Field(example=56)


class ForumPatch(pydantic.BaseModel, extra=pydantic.Extra.forbid):
    name: typing.Optional[str] = pydantic.Field(example="Forum2025")


@router.post("/", response_model=Forum)
async def forum_post(
        post: ForumPost,
        session: models.session.Session = resources.sessions.check_session_dependency,
) -> models.forum.Forum:
    forum = models.forum.Forum(**post.dict(exclude_unset=True), user_id=session.user_id, children=[])
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(forum)
    return forum


@router.patch("/{forum_id}", response_model=Forum)
async def forum_patch(forum_id: str, patch: ForumPatch) -> models.forum.Forum:
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        forum = await models.forum.Forum.get_one(master_transaction, forum_id)
        for key, value in patch.dict(exclude_unset=True).items():
            setattr(forum, key, value)
    return forum


@router.get("/", response_model=ForumGetMany)
async def forum_get_many() -> dict:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        return await resources.base.get_many(slave_transaction, models.forum.Forum, {"parent_id": None})


@router.get("/{forum_id}", response_model=Forum)
async def forum_get_one(forum_id: str) -> models.forum.Forum:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        return await resources.base.get_one(slave_transaction, models.forum.Forum, forum_id)


# FORUM POSTS


class Post(pydantic.BaseModel, orm_mode=True):
    id: int = pydantic.Field(example=123456)
    created: datetime.datetime = pydantic.Field(example=datetime.datetime(2022, 6, 19, 8, 30, 25))
    user: resources.users.UserPublic = pydantic.Field()
    text: str = pydantic.Field(example="I'll kill you")


class PostGetMany(pydantic.BaseModel):
    meta: resources.base.Meta = pydantic.Field()
    items: typing.List[Post] = pydantic.Field()


class PostPost(pydantic.BaseModel, extra=pydantic.Extra.forbid):
    text: str = pydantic.Field(example="I kill 'em all")


class PostPatch(pydantic.BaseModel, extra=pydantic.Extra.forbid):
    text: typing.Optional[str] = pydantic.Field(example="I kill 'em all!!!")


@router.post("/{forum_id}/posts", response_model=Post)
async def forum_post_post(
        forum_id: str, post: PostPost,
        session: models.session.Session = resources.sessions.check_session_dependency,
) -> models.forum.ForumPost:
    post = models.forum.ForumPost(
        **post.dict(exclude_unset=True), forum_id=forum_id, user_id=session.user_id, user=session.user,
    )
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(post)
        return post


@router.patch("/{forum_id}/posts/{post_id}", response_model=Post)
async def forum_post_patch(forum_id: str, post_id: str, patch: PostPatch) -> models.forum.ForumPost:
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        post = await models.forum.ForumPost.get_one(master_transaction, post_id)
        if not post or str(post.forum_id) != forum_id:
            raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
        for key, value in patch.dict(exclude_unset=True).items():
            setattr(post, key, value)
    return post


@router.get("/{forum_id}/posts", response_model=PostGetMany)
async def forum_post_get_many(forum_id: str) -> dict:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        return await resources.base.get_many(slave_transaction, models.forum.ForumPost, {"forum_id": forum_id})


@router.get("/{forum_id}/posts/{post_id}", response_model=Post)
async def forum_post_get_one(forum_id: str, post_id: str) -> models.forum.ForumPost:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        post = await resources.base.get_one(slave_transaction, models.forum.ForumPost, post_id)
        if not post or str(post.forum_id) != forum_id:
            raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
        return post
