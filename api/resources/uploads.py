import asyncio
import datetime
import io
import os
import typing

import PIL.Image
import aiofiles
import fastapi.responses
import pydantic
import sqlalchemy

import databases.mysql
import models.session
import models.uploads
import models.user
import resources.sessions


UPLOADS_DIR: str = os.environ.get("UPLOADS_DIR", "/mafie/uploads")


router = fastapi.APIRouter(tags=["uploads"], dependencies=[resources.sessions.check_session_dependency])


class Upload(pydantic.BaseModel, orm_mode=True):
    id: int = pydantic.Field(example=123456)
    created: datetime.datetime = pydantic.Field(example=datetime.datetime(2022, 6, 19, 8, 30, 25))
    user_id: int = pydantic.Field(example=123)
    filename: str = pydantic.Field(example="picture.jpg")
    url: str = pydantic.Field(example=models.uploads.Upload.generate_url())
    content_type: str = pydantic.Field(example="upload")
    size: int = pydantic.Field(example=2685910)


@router.post("/", response_model=Upload)
async def upload_post(
        file: fastapi.UploadFile,
        session: models.session.Session = resources.sessions.check_session_dependency,
) -> models.uploads.Upload:
    upload = models.uploads.Upload(user_id=session.user.id, filename=file.filename, content_type=file.content_type)
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(upload)
    os.makedirs(f"{UPLOADS_DIR}/{session.user.id}", exist_ok=True)
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(upload)
        async with aiofiles.open(f"{UPLOADS_DIR}/{session.user.id}/{upload.id}", "wb") as out_file:
            while content := await file.read(2**20):
                await out_file.write(content)
                upload.size += len(content)
        upload.status = "done"
    return upload


@router.get("/{upload_id}/{upload_url}", response_model=Upload)
async def upload_get(upload_id: int, upload_url: str) -> fastapi.responses.FileResponse:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        upload = (
            await slave_transaction.execute(sqlalchemy.select(models.uploads.Upload).where(sqlalchemy.and_(
                models.uploads.Upload.id == upload_id,
                models.uploads.Upload.url == upload_url,
            )))).scalars().first()
        if not upload:
            raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
        return fastapi.responses.FileResponse(
            f"{UPLOADS_DIR}/{upload.user_id}/{upload.id}",
            media_type=upload.content_type,
        )


class ImageCrop(pydantic.BaseModel):
    crop_x: int = pydantic.Field(example=200, default=0)
    crop_y: int = pydantic.Field(example=500, default=0)
    crop_w: int = pydantic.Field(example=350, default=None)
    crop_h: int = pydantic.Field(example=450, default=None)


class Image(Upload, ImageCrop):
    width: int = pydantic.Field(example=4000)
    height: int = pydantic.Field(example=3000)


@router.post("/images", response_model=Image)
async def image_post(
        file: fastapi.UploadFile,
        crop: typing.Optional[ImageCrop] = fastapi.Depends(ImageCrop),
        session: models.session.Session = resources.sessions.check_session_dependency,
) -> models.uploads.Image:
    file_content = await file.read()
    loop = asyncio.get_running_loop()
    try:
        pil = PIL.Image.open(io.BytesIO(file_content))
    except PIL.UnidentifiedImageError as exc:
        raise fastapi.HTTPException(fastapi.status.HTTP_415_UNSUPPORTED_MEDIA_TYPE) from exc
    width, height = pil.size
    image = models.uploads.Image(
        user_id=session.user_id, filename=file.filename, content_type=file.content_type, size=len(file_content),
        width=width, height=height, crop_x=crop.crop_x, crop_y=crop.crop_y, crop_w=crop.crop_w, crop_h=crop.crop_h,
    )
    if image.crop_w is None:
        image.crop_w = image.width - image.crop_x
    if image.crop_h is None:
        image.crop_h = image.height - image.crop_y
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(image)
    os.makedirs(f"{UPLOADS_DIR}/{session.user_id}", exist_ok=True)
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        master_transaction.add(image)
        async with aiofiles.open(f"{UPLOADS_DIR}/{session.user_id}/{image.id}", "wb") as out_file:
            await out_file.write(file_content)
        pil.crop((image.crop_x, image.crop_y, image.crop_x + image.crop_w - 1, image.crop_y + image.crop_h - 1))
        loop.run_in_executor(None, pil.save, f"{UPLOADS_DIR}/{session.user_id}/{image.id}-crop")
        image.status = "done"
    return image


@router.get("/images/{image_id}/{image_url}", response_model=Image)
async def image_get(
        image_id: int, image_url: str,
        size_x: typing.Optional[int], size_y: typing.Optional[int],
) -> fastapi.Response:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        image = (
            await slave_transaction.execute(sqlalchemy.select(models.uploads.Image).where(sqlalchemy.and_(
                models.uploads.Image.id == image_id,
                models.uploads.Image.url == image_url,
            )))).scalars().first()
        if not image:
            raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    async with aiofiles.open(f"{UPLOADS_DIR}/{image.user_id}/{image.id}-crop", "rb") as file:
        pil = PIL.Image.open(io.BytesIO(await file.read()))
        pil.resize((size_x, size_y))
        return fastapi.Response(pil.tobytes(), media_type=image.content_type)
