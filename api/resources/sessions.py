import datetime
import typing

import fastapi
import pydantic
import sqlalchemy

import databases.mysql
import models.session
import models.user
import resources.users


router = fastapi.APIRouter(tags=["session"])


async def check_session(
        response: fastapi.Response,
        x_session_id: str = fastapi.Header(None),
        x_session_token: str = fastapi.Header(None),
) -> models.session.Session:
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        session = (await master_transaction.execute(sqlalchemy.select(models.session.Session).where(
            sqlalchemy.and_(
                models.session.Session.id == x_session_id,
                models.session.Session.token == x_session_token,
                models.session.Session.expired > datetime.datetime.now(),
            )
        ))).scalars().first()
        if not session:
            raise fastapi.HTTPException(fastapi.status.HTTP_403_FORBIDDEN)
        session.token = models.session.Session.generate_token()
        response.headers["X-Session-Id"] = str(session.id)
        response.headers["X-Session-Token"] = str(session.token)
        return session


check_session_dependency = fastapi.Depends(check_session)


class Session(pydantic.BaseModel):
    id: int = pydantic.Field(example=123456)
    token: str = pydantic.Field(example="cdUQUwy3zhjF5pbCJq3I3c4lWtlJgzGTcD0IxW8YTM1ySfpByg4SS2v7xi4w")
    created: datetime.datetime = pydantic.Field(example=datetime.datetime(2022, 6, 19, 8, 30, 25))
    expired: datetime.datetime = pydantic.Field(example=datetime.datetime(2022, 6, 19, 8, 50, 25))
    user: resources.users.UserPublic

    class Config:
        orm_mode = True


class SessionPost(pydantic.BaseModel):
    login: str = pydantic.Field(example="gonzales")
    password: str = pydantic.Field(example="strong-pa$$word")


@router.post("/", response_model=Session)
async def session_post(post: SessionPost) -> models.session.Session:
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        user = (await master_transaction.execute(sqlalchemy.select(models.user.UserPublic).where(sqlalchemy.and_(
            models.user.User.id == post.login,
            models.user.User.password == models.user.User.hash_password(post.password),
        )))).scalars().first()
        if not user:
            raise fastapi.HTTPException(fastapi.status.HTTP_403_FORBIDDEN)
        session = models.session.Session(user_id=user.id, user=user)
        master_transaction.add(session)
        return session


@router.get("/", response_model=typing.List[Session])
async def sessions_get(session: models.session.Session = check_session_dependency) -> models.session.Session:
    async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
        return (await slave_transaction.execute(sqlalchemy.select(models.session.Session).where(sqlalchemy.and_(
            models.session.Session.user_id == session.user.id,
            models.session.Session.expired > datetime.datetime.now(),
        )))).scalars().unique().all()


@router.get("/{session_id}", response_model=Session)
async def session_get(session_id: int, session: models.session.Session = check_session_dependency) -> models.session.Session:
    if session_id != session.id:
        async with databases.mysql.SlaveSession.begin() as slave_transaction:  # pylint: disable=no-member
            session = (await slave_transaction.execute(sqlalchemy.select(models.session.Session).where(sqlalchemy.and_(
                models.session.Session.id == session_id,
                models.session.Session.user_id == session.user.id,
                models.session.Session.expired > datetime.datetime.now(),
            )))).scalars().first()
            if not session:
                raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
    return session


@router.delete("/{session_id}", response_model=bool)
async def session_delete(response: fastapi.Response, session_id: int, session: models.session.Session = check_session_dependency) -> bool:
    async with databases.mysql.MasterSession.begin() as master_transaction:  # pylint: disable=no-member
        if session_id != session.id:
            session = (await master_transaction.execute(sqlalchemy.select(models.session.Session).where(sqlalchemy.and_(
                models.session.Session.id == session_id,
                models.session.Session.user_id == session.user.id,
                models.session.Session.expired > datetime.datetime.now(),
            )))).scalars().first()
            if not session:
                raise fastapi.HTTPException(fastapi.status.HTTP_404_NOT_FOUND)
        session.expired = datetime.datetime.now()
        session.token = None
    del response.headers["X-Session-Id"]
    del response.headers["X-Session-Token"]
    return True
