import unittest

import requests

import base


class TestUploads(base.Base):
    BASE_URL = f"{base.Base.API_URL}/uploads"
    FILE_NAME = "uploads/test.txt"

    def test_upload(self):
        with open(self.FILE_NAME, "rb") as file:
            result = self._request(requests.post, self.BASE_URL, files={"file": (self.FILE_NAME, file, "txt")})
            self.assertTrue(result.ok, result)
            upload = result.json()
            file.seek(0)
            content = file.read()
            self.assertEqual(upload["user_id"], self.user["id"])
            self.assertEqual(upload["filename"], self.FILE_NAME)
            self.assertEqual(upload["content_type"], "txt")
            self.assertEqual(upload["size"], len(content))
            result = self._request(requests.get, f"{self.BASE_URL}/{upload['id']}")
            self.assertEqual(result.status_code, 404)
            result = self._request(requests.get, f"{self.BASE_URL}/{upload['id']}/{upload['url']}")
            self.assertTrue(result.ok, result)
            self.assertEqual(result.content, content)


if __name__ == '__main__':
    unittest.main()
