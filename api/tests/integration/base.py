import unittest

import requests


class Base(unittest.TestCase):
    API_URL = "http://mafie-api:8000"
    USER = {
        "name": "User Name",
        "password": "strongest-pa$$word",
        "birthdate": "2008-08-19",
    }
    user = session_id = session_token = None

    def _request(self, method, url, *args, **kwargs):
        kwargs.setdefault("headers", {})
        kwargs["headers"]["X-Session-Id"] = str(self.session_id)
        kwargs["headers"]["X-Session-Token"] = self.session_token
        result = method(url, *args, **kwargs)
        if "x-session-id" in result.headers:
            self.session_id = result.headers["x-session-id"]
        if "x-session-token" in result.headers:
            self.session_token = result.headers["x-session-token"]
        return result

    def _register(self, user):
        result = self._request(requests.post, f"{self.API_URL}/users", json=user)
        self.assertTrue(result.ok, result)
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertIn("id", data)
        self.assertNotIn("password", data)
        self.assertEqual(data["name"], user["name"])
        self.assertEqual(data["birthdate"], user["birthdate"])
        return data

    def _login(self, user_id, password):
        result = self._request(
            requests.post, f"{self.API_URL}/sessions", json={"login": user_id, "password": password},
        )
        self.assertTrue(result.ok, result)
        session = result.json()
        self.assertEqual(session["user"]["id"], user_id)
        self.session_id = session["id"]
        self.session_token = session["token"]

    def setUp(self):
        self.user = self._register(self.USER)
        self._login(self.user["id"], self.USER["password"])
