#!/bin/sh -e

# Wait for mafie startup
echo Waiting for mafie startup...
countdown=12
WGET="docker run --network=mafie apteno/alpine-jq wget"
until $WGET http://mafie-api:8000/ -q -t 1 -T 1 -O /dev/null -o /dev/null ; do
    countdown=$((countdown-1))
    echo Still waiting for mafie startup... countdown=$countdown
    if [ $countdown = 0 ] ; then exit 1 ; fi
    sleep 1
done

# Fill database with data
echo Waiting for mysql database init...
docker exec -i mafie-mariadb mysql --user=mafie --password=insecure mafie-local < data.sql
