import unittest

import requests

import base


class TestUsers(base.Base):
    BASE_URL = f"{base.Base.API_URL}/users"
    USER1 = {
        "name": "Mafioso",
        "password": "strong-pa$$word",
        "birthdate": "2000-06-19",
    }
    USER2 = {
        "name": "MafiosoX",
        "password": "strong-pa$$wordX",
        "birthdate": "2010-10-09",
    }

    def _get(self, user_id):
        result = self._request(requests.get, f"{self.BASE_URL}/{user_id}")
        self.assertTrue(result.ok, result)
        return result.json()

    def _patch(self, user_id, user):
        result = self._request(requests.patch, f"{self.BASE_URL}/{user_id}", json=user)
        self.assertTrue(result.ok, result)
        return result.json()

    def test_register(self):
        user1a = self._register(self.USER1)
        user1b = self._register(self.USER1)
        self.assertNotEqual(user1a["id"], user1b["id"])
        self.assertNotEqual(user1a, user1b)
        self.assertDictEqual(user1a, self._get(user1a["id"]))
        self.assertDictEqual(user1b, self._get(user1b["id"]))

    def test_edit(self):
        user = self._register(self.USER1)
        user_same = self._patch(user["id"], self.USER1)
        self.assertDictEqual(user, user_same)
        self.assertDictEqual(user, self._get(user["id"]))
        user_edited = self._patch(user["id"], self.USER2)
        for key in user.keys():
            if key in {"id", "registered", "image", "emails", "phones"}:
                self.assertEqual(user[key], user_edited[key])
            else:
                self.assertNotEqual(user_edited[key], user[key])
                self.assertEqual(user_edited[key], self.USER2[key])
        for key in {"id", "registered", "image", "emails", "phones", "unknown"}:
            with self.subTest("unknown", key=key):
                result = self._request(requests.patch, f'{self.BASE_URL}/{user["id"]}', json={key: user["id"] + 1})
                self.assertFalse(result.ok, result)
                self.assertEqual(result.status_code, 422)

    def _test_email_phone(self, name, value):
        user = self._register(self.USER1)
        # Add unverified email/phone
        result = self._request(requests.put, f'{self.BASE_URL}/{user["id"]}/{name}s/{value}')
        self.assertEqual(result.status_code, 200, result)
        email_phone = result.json()
        self.assertEqual(email_phone[name], value, email_phone)
        self.assertIn("verifications", email_phone)
        self.assertEqual(len(email_phone["verifications"]), 1)
        verification = email_phone["verifications"][0]
        self.assertIsNone(verification["verified"], verification)
        self.assertNotIn("code", verification, verification)
        # Verify email/phone
        result = self._request(requests.post, f'{self.BASE_URL}/{user["id"]}/{name}s/{value}/verify/12345678')
        self.assertEqual(result.status_code, 200, result)
        verification = result.json()
        self.assertIsNotNone(verification["verified"], verification)
        self.assertNotIn("code", verification, verification)

    @unittest.skip("TODO: verification")
    def test_email(self):
        return self._test_email_phone("email", "email@mafie.cz")

    @unittest.skip("TODO: verification")
    def test_phone(self):
        return self._test_email_phone("phone", "+420777123456")


if __name__ == '__main__':
    unittest.main()
