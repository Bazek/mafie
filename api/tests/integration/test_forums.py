import unittest

import requests

import base


class TestForums(base.Base):
    maxDiff = None
    BASE_URL = f"{base.Base.API_URL}/forums"

    def test_tree(self):
        forums_dict = {}
        for i in range(2):
            result = self._request(requests.post, self.BASE_URL, json={"name": f"TestForum-{i}"})
            self.assertTrue(result.ok, result)
            forum = result.json()
            forums_dict[forum["id"]] = forum
            forum["children"] = []
            for j in range(3):
                result = self._request(requests.post, self.BASE_URL, json={
                    "name": f"TestForum-{i}-{j}", "parent_id": forum["id"],
                })
                self.assertTrue(result.ok, result)
                child = result.json()
                forum["children"].append(child)
                forums_dict[child["id"]] = child
                child["children"] = []
                for k in range(2):
                    result = self._request(requests.post, self.BASE_URL, json={
                        "name": f"TestForum-{i}-{j}-{k}", "parent_id": child["id"],
                    })
                    self.assertTrue(result.ok, result)
                    child2 = result.json()
                    child["children"].append(child2)
                    forums_dict[child2["id"]] = child2
        for forum_id, forum in forums_dict.items():
            result = self._request(requests.get, f"{self.BASE_URL}/{forum_id}")
            self.assertTrue(result.ok, result)
            self.assertDictEqual(result.json(), forum)

    def test_posts(self):
        result = self._request(requests.post, self.BASE_URL, json={"name": "MyUnitTestForum"})
        self.assertTrue(result.ok, result)
        forum = result.json()
        result = self._request(requests.get, f"{self.BASE_URL}/{forum['id']}/posts")
        self.assertTrue(result.ok, result)
        self.assertListEqual(result.json()["items"], [])
        result = self._request(requests.post, f"{self.BASE_URL}/{forum['id']}/posts", json={"text": "MyUnitTestTEXT"})
        self.assertTrue(result.ok, result)
        post = result.json()
        self.assertEqual(post["text"], "MyUnitTestTEXT")
        result = self._request(requests.get, f"{self.BASE_URL}/{forum['id']}/posts/{post['id']}")
        self.assertTrue(result.ok, result)
        self.assertEqual(result.json()["text"], "MyUnitTestTEXT")
        # result = self._request(requests.get, f"{self.BASE_URL}/{forum['id'] + 1}/posts/{post['id']}")
        # self.assertFalse(result.ok, result)
        # self.assertEqual(result.status_code, 404)
        result = self._request(requests.patch, f"{self.BASE_URL}/{forum['id']}/posts/{post['id']}", json={"text": "ChangedTEXT"})
        self.assertTrue(result.ok, result)
        post = result.json()
        self.assertEqual(post["text"], "ChangedTEXT")
        result = self._request(requests.get, f"{self.BASE_URL}/{forum['id']}/posts/{post['id']}")
        self.assertTrue(result.ok, result)
        self.assertEqual(result.json()["text"], "ChangedTEXT")


if __name__ == '__main__':
    unittest.main()
