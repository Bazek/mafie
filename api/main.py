import os

import aiofiles
import fastapi.middleware.cors
import sqlalchemy.exc

import models.base
import resources.forums
import resources.sessions
import resources.uploads
import resources.users


ROOT_PATH = os.environ.get("API_ROOT_PATH", "")
SERVERS = [{"url": url} for url in os.environ.get("API_SERVERS", "").split()]
METRICS_BUCKETS = [
    float(bucket)
    for bucket in os.environ.get("API_METRICS_BUCKETS", "0.005 0.010 0.050 0.100").split()
]


# Main api and root router
api = fastapi.FastAPI(
    title="Mafie", description="Mafie API",
    root_path=ROOT_PATH, servers=SERVERS, docs_url="/",
)
api.add_middleware(
    fastapi.middleware.cors.CORSMiddleware,
    allow_origins=["*"], allow_methods=["*"], allow_headers=["*"],
    expose_headers=["X-Session-Id", "X-Session-Token"],
)


@api.exception_handler(sqlalchemy.exc.IntegrityError)
async def integrity_error(request, exc):  # pylint: disable=unused-argument
    return fastapi.responses.JSONResponse({"msg": repr(exc)}, status_code=fastapi.status.HTTP_409_CONFLICT)


async def get_version(filename="/mafie/api/VERSION"):
    async with aiofiles.open(filename, mode="r") as f:
        version = await f.read()
        return version.strip()


@api.on_event("startup")
async def startup():
    api.version = await get_version()
    await models.base.ModelBase.init_database()


# Include resources routers
api.router.include_router(resources.sessions.router, prefix="/sessions")
api.router.include_router(resources.uploads.router, prefix="/uploads")
api.router.include_router(resources.users.router, prefix="/users")
api.router.include_router(resources.forums.router, prefix="/forums")
