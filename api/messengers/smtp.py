import email.message
import os

import aiosmtplib


class SMTP:
    SMTP_SERVER: str = os.environ.get("SMTP_SERVER")
    SMTP_LOGIN: str = os.environ.get("SMTP_LOGIN")
    SMTP_PASSWORD: str = os.environ.get("SMTP_PASSWORD")
    EMAIL_FROM: str = os.environ.get("EMAIL_FROM")
    EMAIL_SENDER: str = os.environ.get("EMAIL_SENDER")

    async def send(self, recipients, subject, content):
        if not isinstance(recipients, (list, tuple, set)):
            recipients = [recipients]
        msg = email.message.EmailMessage()
        msg.set_content(content)
        msg["From"] = self.EMAIL_FROM
        msg["To"] = recipients
        msg["Subject"] = subject
        async with aiosmtplib.SMTP(self.SMTP_SERVER) as smtp_server:
            await smtp_server.login(self.SMTP_LOGIN, self.SMTP_PASSWORD)
            await smtp_server.send_message(msg, self.EMAIL_SENDER, recipients)


smtp = SMTP()
