import os

import aiohttp


class Twilio:
    # Your Account SID from twilio.com/console
    ACCOUNT_SID: str = os.environ.get("TWILIO_SID")
    # Your Auth Token from twilio.com/console
    AUTH_TOKEN: str = os.environ.get("TWILIO_TOKEN")
    # Phone number which we use
    PHONE_NUMBER: str = os.environ.get("TWILIO_PHONE")

    def __init__(self):
        super().__init__()
        self.auth = aiohttp.BasicAuth(login=self.ACCOUNT_SID, password=self.AUTH_TOKEN)

    async def send_sms(self, recipient, body, from_=PHONE_NUMBER):
        async with aiohttp.ClientSession(auth=self.auth) as session:
            result = await session.post(
                f'https://api.twilio.com/2010-04-01/Accounts/{self.ACCOUNT_SID}/Messages.json',
                data={'From': from_, 'To': recipient, 'Body': body},
            )
            return result


twilio = Twilio()
