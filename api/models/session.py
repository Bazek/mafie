import datetime
import random
import string

import sqlalchemy.orm

import models.base
import models.user


class Session(models.base.ModelBase):
    __tablename__ = "sessions"

    @staticmethod
    def generate_token(length=128, chars=string.ascii_letters + string.digits):
        return "".join(random.choice(chars) for _ in range(length))

    @staticmethod
    def generate_expired(minutes=40):
        return datetime.datetime.now() + datetime.timedelta(minutes=minutes)

    id = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, autoincrement=True, primary_key=True)
    token = sqlalchemy.Column(sqlalchemy.String(128), nullable=False, primary_key=True, default=generate_token)
    user_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(models.user.User.id), nullable=False)
    created = sqlalchemy.Column(models.base.DateTime, nullable=False, default=datetime.datetime.now)
    expired = sqlalchemy.Column(models.base.DateTime, nullable=False, default=generate_expired, onupdate=generate_expired)

    user = sqlalchemy.orm.relation(models.user.UserPublic, lazy="joined")
