import datetime
import random
import string

import sqlalchemy.orm

import models.base
import models.user


class Upload(models.base.ModelBase):
    __tablename__ = "uploads"

    @staticmethod
    def generate_url(length=32, chars=string.ascii_letters + string.digits):
        return "".join(random.choice(chars) for _ in range(length))

    id = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, autoincrement=True, primary_key=True)
    created = sqlalchemy.Column(models.base.DateTime, nullable=False, default=datetime.datetime.now)
    user_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(models.user.User.id), nullable=False)
    filename = sqlalchemy.Column(sqlalchemy.String(256), nullable=False)
    url = sqlalchemy.Column(sqlalchemy.String(128), nullable=False, default=generate_url)
    content_type = sqlalchemy.Column(sqlalchemy.String(64), nullable=False)
    status = sqlalchemy.Column(sqlalchemy.Enum("uploading", "saving", "done"), nullable=False, default="saving")
    size = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, default=0)

    user = sqlalchemy.orm.relation(models.user.User, lazy="joined", join_depth=1)


class Image(Upload):
    __tablename__ = "images"

    upload_id = sqlalchemy.Column(sqlalchemy.ForeignKey(Upload.id), nullable=False, primary_key=True)
    width = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False)
    height = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False)
    crop_x = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, default=0)
    crop_y = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, default=0)
    crop_w = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False)
    crop_h = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False)
