import datetime

import sqlalchemy.orm

import models.base
import models.user


class Forum(models.base.ModelBase):
    __tablename__ = "forums"

    id = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, autoincrement=True, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String(64), nullable=False)
    created = sqlalchemy.Column(models.base.DateTime, nullable=False, default=datetime.datetime.now)
    user_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(models.user.User.id), nullable=False)
    parent_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(id), nullable=True)

    user = sqlalchemy.orm.relation(models.user.User, remote_side=[models.user.User.id])
    parent = sqlalchemy.orm.relation("Forum", remote_side=[id])
    children = sqlalchemy.orm.relation("Forum", lazy="joined", join_depth=5)


class ForumPost(models.base.ModelBase):
    __tablename__ = "forums_posts"

    id = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, autoincrement=True, primary_key=True)
    created = sqlalchemy.Column(models.base.DateTime, nullable=False, default=datetime.datetime.now)
    user_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(models.user.User.id), nullable=False)
    forum_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(Forum.id), nullable=True)
    text = sqlalchemy.Column(sqlalchemy.Text(), nullable=False)

    forum = sqlalchemy.orm.relation(Forum, remote_side=[Forum.id])
    user = sqlalchemy.orm.relation(models.user.UserPublic, remote_side=[models.user.UserPublic.id], lazy="joined")
