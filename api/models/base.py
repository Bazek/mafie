import asyncio
import typing
import os

import sqlalchemy
import sqlalchemy.dialects.mysql
import sqlalchemy.exc
import sqlalchemy.ext.declarative

import databases.mysql


INIT_RETRIES = int(os.environ.get("MYSQL_INIT_RETRIES", 40))
INIT_SLEEP = float(os.environ.get("MYSQL_INIT_SLEEP", 0.8))


DeclarativeBase = sqlalchemy.ext.declarative.declarative_base()

DateTime = sqlalchemy.dialects.mysql.DATETIME(fsp=6)


class ModelBase(DeclarativeBase):
    __abstract__ = True

    # Create all schema in DB
    @staticmethod
    async def init_database(drop=False):
        for _ in range(INIT_RETRIES):
            try:
                async with databases.mysql.master_engine.begin() as master:
                    if drop:
                        await master.run_sync(DeclarativeBase.metadata.drop_all)
                    await master.run_sync(DeclarativeBase.metadata.create_all)
            except sqlalchemy.exc.OperationalError as e:
                print(e)
                await asyncio.sleep(INIT_SLEEP)
            else:
                break

    @classmethod
    def _add_filter(cls, query, filter_dict: dict):
        filter_dict = filter_dict or {}
        for name, value in filter_dict.items():
            field = getattr(cls, name, None)
            if field:
                if isinstance(value, dict) and value.get("$regex"):
                    query = query.filter(field.op('regexp')(value["$regex"]))
                else:
                    query = query.where(field == value)
        return query

    @classmethod
    def _add_order(cls, query, order: str = None):
        if order:
            for name in order.split(","):
                name, direction = name.strip(), sqlalchemy.asc
                if name and name[0] == "-":
                    name, direction = name[1:], sqlalchemy.desc
                field = getattr(cls, name, None)
                if field:
                    query = query.order_by(direction(field))
        return query

    @classmethod
    def _add_limit(cls, query, limit: int = None):
        if limit is not None:
            query = query.limit(limit)
        return query

    @classmethod
    def _add_offset(cls, query, offset: int = None):
        if offset is not None:
            query = query.offset(offset)
        return query

    @classmethod
    async def get_many(
            cls, transaction, filter_dict: dict = None, order: str = None, limit=None, offset=None,
    ) -> typing.Tuple[typing.List[DeclarativeBase], int]:
        query = sqlalchemy.select(cls)
        query = cls._add_filter(query, filter_dict)
        query = cls._add_order(query, order)
        query = cls._add_limit(query, limit)
        query = cls._add_offset(query, offset)
        result = await transaction.execute(query)
        query_count = sqlalchemy.select(sqlalchemy.func.count(cls.id))
        query_count = cls._add_filter(query_count, filter_dict)
        result_count = await transaction.execute(query_count)
        return result.scalars().unique().all(), result_count.scalar()

    @classmethod
    async def get_one(cls, transaction, item_id: str, id_field: str = "id") -> DeclarativeBase:
        if item_id.isdigit():
            query = sqlalchemy.select(cls).where(getattr(cls, id_field) == item_id)
            result = await transaction.execute(query)
            item = result.scalars().first()
            if item:
                return item
