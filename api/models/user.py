import datetime
import hashlib
import random
import string

import sqlalchemy.orm

import models.base


class UserPublic(models.base.ModelBase):
    __tablename__ = "users"

    id = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, autoincrement=True, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String(64), nullable=False)
    image_id = sqlalchemy.Column(sqlalchemy.Integer(), nullable=True)

    image = sqlalchemy.orm.relation(
        "Image", foreign_keys=image_id, primaryjoin="User.image_id == Image.id", lazy="joined", join_depth=2,
    )


class User(UserPublic):

    password = sqlalchemy.Column(sqlalchemy.String(128), nullable=False)
    registered = sqlalchemy.Column(models.base.DateTime, nullable=False, default=datetime.datetime.now)
    birthdate = sqlalchemy.Column(sqlalchemy.Date(), nullable=False)
    emails = sqlalchemy.orm.relation("UserEmail", lazy="joined")
    phones = sqlalchemy.orm.relation("UserPhone", lazy="joined")

    @staticmethod
    def hash_password(password: str) -> str:
        return hashlib.sha256(password.encode()).hexdigest()


class UserEmail(models.base.ModelBase):
    __tablename__ = "users_emails"
    user_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(User.id), nullable=False, primary_key=True)
    email = sqlalchemy.Column(sqlalchemy.String(64), nullable=False, primary_key=True)
    verifications = sqlalchemy.orm.relation("UserEmailVerification", lazy="joined")


class UserPhone(models.base.ModelBase):
    __tablename__ = "users_phones"
    user_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(User.id), nullable=False, primary_key=True)
    phone = sqlalchemy.Column(sqlalchemy.String(32), nullable=False, primary_key=True)
    verifications = sqlalchemy.orm.relation("UserPhoneVerification", lazy="joined")


class Verification(models.base.ModelBase):
    __abstract__ = True

    @staticmethod
    def generate_code(length=8, chars=string.ascii_uppercase + string.digits):
        return "".join(random.choice(chars) for _ in range(length))

    code = sqlalchemy.Column(sqlalchemy.String(32), nullable=False, default=generate_code)
    generated = sqlalchemy.Column(models.base.DateTime, nullable=False, default=datetime.datetime.now)
    verified = sqlalchemy.Column(models.base.DateTime, nullable=True, default=lambda: None)


class UserEmailVerification(Verification):
    __tablename__ = "users_emails_verifications"

    user_id = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, primary_key=True)
    email = sqlalchemy.Column(sqlalchemy.String(64), nullable=False, primary_key=True)
    sqlalchemy.ForeignKeyConstraint((user_id, email), (UserEmail.user_id, UserEmail.email))


class UserPhoneVerification(Verification):
    __tablename__ = "users_phones_verifications"

    user_id = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False, primary_key=True)
    phone = sqlalchemy.Column(sqlalchemy.String(32), nullable=False, primary_key=True)
    sqlalchemy.ForeignKeyConstraint((user_id, phone), (UserPhone.user_id, UserPhone.phone))
